
# set terminal png transparent nocrop enhanced size 640,480 font "arial,8" 
 #set terminal png  nocrop enhanced size 640,480 font "arial,8" 
 #set output 'output.png'
reset
set terminal postscript  eps size 3.5,2.62 enhanced color font 'Helvetica,12'
set output 'stress.eps'
set bmargin 6
set lmargin 7
set rmargin 12
set style line 2 linecolor rgb 'black' linetype 1 linewidth 4
set style line 3 linecolor rgb 'red' linetype 1 linewidth 4
set style line 4 linecolor rgb 'blue' linetype 1 linewidth  4
set style line 5 linecolor rgb 'orange' linetype 1 linewidth 4
set style line 6 linecolor rgb 'purple' linetype 1 linewidth  4

 set xrange [37.7:39.4]
  set yrange [-0.3:-0.12]
# set x2range [0:6]
# 
# set yrange [0:8]
# set y2range [38:42]
# set autoscale y2
# set y2tics 30,2
# set xlabel "~{/Symbol l} _2" font "Helvetica,28"
# set ylabel "n_c" font "Helvetica,34"
# set y2label "~{/Symbol e}^-_c"  font "Helvetica,34"

set label 3 center
#set key center right

set key font ",22"

set xtics font ", 18"
set ytics font ", 18"
set xtics 37.8,0.5,39.8

set ytics -0.3,0.05,-0.12

#plot 'n_cri.dat'  using 1:2 axes x1y1 with lines linestyle 3 title  "n_c" ,\
#'e_cri1.dat' using 1:2 axes x2y2 with lines linestyle 2 title "~{/Symbol e}^-*_c"  ,\
#'e_cri2.dat' using 1:2 axes x2y2 with lines linestyle 4 title "~{/Symbol e}^-**_c" 

 f(x) = 2./(1. + 40. - x)**3 - 2/(1 + 40 - x)**2 

#f(x) = 1./(1. + 40. - x)**2 - 2./(1. + 40 - x)
 plot f(x)  with lines linestyle 2 notitle  ,\
'n3.dat'  using 1:8   axes x1y1 with lines linestyle 4 title  "n=3" ,\
'n4.dat'  using 1:8 axes x1y1 with lines linestyle 3	 title  "n=4" ,\
'n5.dat'  using 1:8 axes x1y1 with lines linestyle 5	 title  "n=5" ,\
'n6.dat'  using 1:8 axes x1y1 with lines linestyle 6	 title  "n=6" 
###########################################################################################
##########################################################################################
##########################################################################################
reset
set terminal postscript  eps size 3.5,2.62 enhanced color font 'Helvetica,12'
set output 'deltaenergy.eps'
set bmargin 6
set lmargin 7
set rmargin 12
set style line 2 linecolor rgb 'black' linetype 1 linewidth 4
set style line 3 linecolor rgb 'red' linetype 1 linewidth 4
set style line 4 linecolor rgb 'blue' linetype 1 linewidth  4
set style line 5 linecolor rgb 'orange' linetype 1 linewidth 4
set style line 6 linecolor rgb 'purple' linetype 1 linewidth  4

 set xrange [37.5:39.8]


f(x)=0
 plot f(x)  with lines linestyle 2 notitle  ,\
'n3.dat'  using 1:7   axes x1y1 with lines linestyle 4 title  "n=3" ,\
'n4.dat'  using 1:7 axes x1y1 with lines linestyle 3	 title  "n=4" ,\
'n5.dat'  using 1:7 axes x1y1 with lines linestyle 5	 title  "n=5" ,\
'n6.dat'  using 1:7 axes x1y1 with lines linestyle 6	 title  "n=6" 





##########################################################################################
##########################################################################################
##########################################################################################

reset
set terminal postscript  eps size 3.5,2.62 enhanced color font 'Helvetica,12'
set output 'deltaenergy.eps'
set bmargin 6
set lmargin 11 
set rmargin 8
set style line 2 linecolor rgb 'black' linetype 1 linewidth 4
set style line 3 linecolor rgb 'red' linetype 1 linewidth 4
set style line 4 linecolor rgb 'blue' linetype 1 linewidth  4
set style line 5 linecolor rgb 'orange' linetype 1 linewidth 4
set style line 6 linecolor rgb 'purple' linetype 1 linewidth  4

set xrange [37.8:39.4]
set yrange [-0.009:0.0021]

set xtics font ", 18"
set ytics font ", 18"
set xtics 37.8,0.5,39.8

set ytics -0.009,0.002,0.0021

set key top right

f(x)=0
 plot f(x)  with lines linestyle 2 notitle  ,\
'n3.dat'  using 1:7   axes x1y1 with lines linestyle 4 title  "n=3" ,\
'n4.dat'  using 1:7 axes x1y1 with lines linestyle 3	 title  "n=4" ,\
'n5.dat'  using 1:7 axes x1y1 with lines linestyle 5	 title  "n=5" ,\
'n6.dat'  using 1:7 axes x1y1 with lines linestyle 6	 title  "n=6" 





##########################################################################################
##########################################################################################
##########################################################################################



reset
set terminal postscript  eps size 3.5,2.62 enhanced color font 'Helvetica,12'
set output 'profile.eps'
set bmargin 6
set lmargin 11
set rmargin 12
set style line 2 linecolor rgb 'black' linetype 1 linewidth 4
set style line 3 linecolor rgb 'red' linetype 1 linewidth 4
set style line 4 linecolor rgb 'blue' linetype 1 linewidth  4
set style line 5 linecolor rgb 'orange' linetype 1 linewidth 4
set style line 6 linecolor rgb 'purple' linetype 1 linewidth  4

 set xrange [0:1]
 set yrange [36.5:40]
set xtics font ", 28"
set ytics font ", 28"
set xtics 0,.5,1

set ytics 37,1,40

 plot 'n4p23.dat'  using 1:3   axes x1y1 with lines linestyle 3 notitle  ,\
'n4p26.dat'  using 1:3 axes x1y1 with lines linestyle 3	 notitle  





















# reset
# set terminal postscript  eps size 3.5,2.62 enhanced color font 'Helvetica,12'
# set output 'fig3c.eps'
# set bmargin 6
# set lmargin 7
# set rmargin 12
# set style line 2 linecolor rgb 'black' linetype 1 linewidth 6
# set style line 3 linecolor rgb 'red' linetype 1 linewidth 6
# set style line 4 linecolor rgb 'blue' linetype 1 linewidth 6
# set style line 5 linecolor rgb 'brown' linetype 1 linewidth 6
# set style line 6 linecolor rgb 'purple' linetype 1 linewidth 6
# 
#  set xrange [37.6:39.5]
# 
# f(x)=  ((1. + 40. -  x)**(-2) - 2./(1. + 40. -  x))
# 
#  plot f(x)  with lines linestyle 2 notitle  ,\
#  'n5_2.dat'  using 1:8 axes x1y1 with lines linestyle 5 title  "n=5" 
# 
# 
# 
# reset
# set terminal postscript  eps size 3.5,2.62 enhanced color font 'Helvetica,12'
# set output 'fig4c.eps'
# set bmargin 6
# set lmargin 7
# set rmargin 12
# set style line 2 linecolor rgb 'black' linetype 1 linewidth 6
# set style line 3 linecolor rgb 'red' linetype 1 linewidth 6
# set style line 4 linecolor rgb 'blue' linetype 1 linewidth 6
# set style line 5 linecolor rgb 'brown' linetype 1 linewidth 6
# set style line 6 linecolor rgb 'purple' linetype 1 linewidth 6
# 
#  set xrange [37.6:39.5]
# 
# 
# 
#  plot  'n5_2.dat'  using 1:7 axes x1y1 with lines linestyle 5 title  "n=5" 

# set key at 19,0.28 
# set lmargin 10
# set bmargin 5
# 
# # Line width of the axes
# set border linewidth 1.5
# # Line styles
# # Line styles
# set style line 1 linecolor rgb 'black' linetype 1 linewidth 2 
# set style line 2 linecolor rgb 'red' linetype 1 linewidth 2 
# set style line 3 linecolor rgb 'blue' linetype 1 linewidth 2 
# set style line 4 linecolor rgb 'green' linetype 1 linewidth 2
# set style line 5 linecolor rgb 'orange' linetype 1 linewidth 2
# 
# 
# set xlabel "~{/Symbol e}{.3-}" font "Helvetica,25"
# set ylabel "{/Symbol s}" font "Helvetica,25"
# 
# 
# 
# plot[0:20] 'stressb0.dat'  using 1:8 with lines linestyle 1 title "homogenous", \
#      'stressb1.dat'  using 1:8 with lines linestyle 2 title "n=1", \
#      'stressb2.dat'  using 1:8 with lines linestyle 3 title "n=2", 
#      
     
# plot 'sol23_br3.dat'  using 1:3 with lines linestyle 3 notitle, \
# 'sol29_br3.dat'  using 1:3 with lines linestyle 3 notitle, \
# 'sol33_br3.dat'  using 1:3 with lines linestyle 3 notitle,
# 'sol32.dat'  using 1:3 with lines linestyle 3 notitle, \
# 'sol40.dat'  using 1:3 with lines linestyle 3 notitle, \
# 'sol50.dat'  using 1:3 with lines linestyle 3 notitle