!---------------------------------------------------------------------- 
!---------------------------------------------------------------------- 
!   bvp :    A nonlinear ODE eigenvalue problem
!---------------------------------------------------------------------- 
!---------------------------------------------------------------------- 

SUBROUTINE FUNC(NDIM,U,ICP,PAR,IJAC,F,DFDU,DFDP) 
!--------- ---- 

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: NDIM, IJAC, ICP(*)
  DOUBLE PRECISION, INTENT(IN) :: U(NDIM), PAR(*)
  DOUBLE PRECISION, INTENT(OUT) :: F(NDIM)
  DOUBLE PRECISION, INTENT(INOUT) :: DFDU(NDIM,*), DFDP(NDIM,*)

  DOUBLE PRECISION pi
  DOUBLE PRECISION X
  DOUBLE PRECISION e0

  pi = 4.d0*ATAN(1.d0)
  e0 = 40.d0

  F(1) = U(2)
  F(2) = U(3)
  F(3) = U(4)
  F(4) = ( (6./(1. + e0 - U(2))**4 - 4./(1. + e0 - U(2))**3   )*U(3)  - PAR(9)*(U(1)-U(5)) )&
   / PAR(8)
  F(5) = U(6)
  F(6) = 0.

END SUBROUTINE FUNC

SUBROUTINE STPNT(NDIM,U,PAR,T) 
!--------- ----- 

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: NDIM
  DOUBLE PRECISION, INTENT(INOUT) :: U(NDIM), PAR(*)
  DOUBLE PRECISION, INTENT(IN) :: T

  PAR(8) =  0.00028
  PAR(9) =  (1./0.45)**2
  PAR(4) =  37.6 

  U = 0.0d0

END SUBROUTINE STPNT

SUBROUTINE BCND(NDIM,PAR,ICP,NBC,U0,U1,FB,IJAC,DBC) 
!--------- ---- 

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: NDIM, ICP(*), NBC, IJAC
  DOUBLE PRECISION, INTENT(IN) :: PAR(*), U0(NDIM), U1(NDIM)
  DOUBLE PRECISION, INTENT(OUT) :: FB(NBC)
  DOUBLE PRECISION, INTENT(INOUT) :: DBC(NBC,*)

!  FB = (/ U0(1)+PAR(1)/2, U1(1)-PAR(1)/2, U0(3), U1(3) /)
       FB(1)=U0(1)+PAR(4)/2.
       FB(2)=U1(1)-PAR(4)/2.
       FB(3)=U0(3)
       FB(4)=U1(3)
       FB(5)=U0(5)+PAR(4)/2
       FB(6)=U1(5)-PAR(4)/2

!       FB(1)=U0(2)
!       FB(2)=U1(2) 
!       FB(3)=U0(4)
!       FB(4)=U1(4)

END SUBROUTINE BCND

SUBROUTINE ICND(NDIM,PAR,ICP,NINT,U,UOLD,UDOT,UPOLD,FI,IJAC,DINT)
!--------- ----

!  IMPLICIT NONE
!  INTEGER, INTENT(IN) :: NDIM, ICP(*), NINT, IJAC
!  DOUBLE PRECISION, INTENT(IN) :: PAR(*)
!  DOUBLE PRECISION, INTENT(IN) :: U(NDIM), UOLD(NDIM), UDOT(NDIM), UPOLD(NDIM)
!  DOUBLE PRECISION, INTENT(OUT) :: FI(NINT)
!  DOUBLE PRECISION, INTENT(INOUT) :: DINT(NINT,*)
!
!  FI = (/ U(1)-PAR(2) /)

END SUBROUTINE ICND

SUBROUTINE FOPT 

END SUBROUTINE FOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!RETURNS ENERGY
DOUBLE PRECISION FUNCTION GETU2(U,NDX,NTST,NCOL,WI,DTM,PAR)
!     ------ --------- -------- -----
      INTEGER, INTENT(IN) :: NDX,NCOL,NTST
      DOUBLE PRECISION, INTENT(IN) :: U(NDX,0:NCOL*NTST),WI(0:NCOL),DTM(NTST)
      DOUBLE PRECISION, INTENT(IN) :: PAR(*)
      DOUBLE PRECISION :: size,sum

! ------ --------- -------- -----

! Local
    INTEGER J,K,IC
    DOUBLE PRECISION S
    DOUBLE PRECISION  e0
    DOUBLE PRECISION,    dimension(:), allocatable :: X,FX
    allocate(X(K:K+NCOL),FX(K:K+NCOL))

	e0 =40
    S=0.d0
    K=0
    !!STRAIN, first derivative of U
    IC=2
    DO J=1,NTST
!       X (0:NCOL)  = 1+U(IC,K:K+NCOL)
!       FX(0:NCOL)  = X(0:NCOL)**(-2.)-2.*X(0:NCOL)**(-1) + 0.0005*U(3,K:K+NCOL)**(2.)/2.

       X (0:NCOL)  = U(IC,K:K+NCOL)
!       FX(0:NCOL)  = X(0:NCOL)/(2+ X (0:NCOL)) +  PAR(6)*U(3,K:K+NCOL)**(2.)/2.
	   FX(0:NCOL)  = (1. + e0 -  X(0:NCOL))**(-2) - 2./(1. + e0 -  X(0:NCOL)) +  PAR(8)*U(3,K:K+NCOL)**(2.)/2.+       & 
       PAR(9)*(U(1,K:K+NCOL)-U(5,K:K+NCOL))**(2.)/2. 
!       S=S+DTM(J)*DOT_PRODUCT(WI,U(IC,K:K+NCOL))
       S=S+DTM(J)*DOT_PRODUCT(WI,FX(0:NCOL))
       K=K+NCOL
    END DO
    
!!homogenous solution



deallocate(X,FX)

    GETU2= S - ((1. + e0 -  PAR(4))**(-2) - 2./(1. + e0 -  PAR(4)))  


END FUNCTION GETU2
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!RETURNS STRESS
DOUBLE PRECISION FUNCTION GETU3(U,NDX,NTST,NCOL,WI,DTM,PAR)
!     ------ --------- -------- -----
      INTEGER, INTENT(IN) :: NDX,NCOL,NTST
      DOUBLE PRECISION, INTENT(IN) :: U(NDX,0:NCOL*NTST),WI(0:NCOL),DTM(NTST)
      DOUBLE PRECISION, INTENT(IN) :: PAR(*)
      DOUBLE PRECISION :: size,sum

! ------ --------- -------- -----

! Local
    INTEGER J,K,IC
    DOUBLE PRECISION S
    DOUBLE PRECISION e0
    DOUBLE PRECISION,    dimension(:), allocatable :: X,FX
    allocate(X(K:K+NCOL),FX(K:K+NCOL))



    S=0.d0
    K=0
    e0 = 40.
    IC=2
    DO J=1,NTST
!       X (0:NCOL)  = 1+U(IC,K:K+NCOL)
!       FX(0:NCOL)  = -2.*X(0:NCOL)**(-3.)+2.*X(0:NCOL)**(-2) + 0.0005*U(4,K:K+NCOL)

       X (0:NCOL)  = U(IC,K:K+NCOL)
!        FX(0:NCOL)  = 4*X(0:NCOL)/(2+X (0:NCOL)**2)**2   +  PAR(6)*U(4,K:K+NCOL)
       FX(0:NCOL)  = 2./(1. + e0 - X (0:NCOL))**3 - 2./(1. + e0 - X (0:NCOL))**2 -  PAR(8)*U(4,K:K+NCOL) &
       - 0*PAR(9)*(U(1,K:K+NCOL)-U(5,K:K+NCOL))
!        FX(0:NCOL)  = (6./(1. + e0 - X (0:NCOL))**4 - 4./(1. + e0 - X (0:NCOL))**3   )*U(3,K:K+NCOL) &
!        -PAR(8)*U(5,K:K+NCOL) - PAR(9)*(U(1,K:K+NCOL)-U(5,K:K+NCOL))
           
   
       
!      S=S+DTM(J)*DOT_PRODUCT(WI,U(IC,K:K+NCOL))
       S=S+DTM(J)*DOT_PRODUCT(WI,FX(0:NCOL))
       K=K+NCOL
    END DO

deallocate(X,FX)

    GETU3=S


END FUNCTION GETU3

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      SUBROUTINE PVLS(NDIM,U,PAR)
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: NDIM
      DOUBLE PRECISION, INTENT(IN) :: U(NDIM)
      !DOUBLE PRECISION, INTENT(IN) :: U(NDX,0:NCOL*NTST)

      DOUBLE PRECISION, INTENT(INOUT) :: PAR(*)
            
      INTEGER NDX,NCOL,NTST,i
     
      DOUBLE PRECISION, EXTERNAL :: GETP,GETU2,GETU3
      DOUBLE PRECISION :: RIGHT,LEFT
      DOUBLE PRECISION,    dimension(:), allocatable :: WI, DTM
 

       NDX=NINT(GETP('NDX',0,U))
       NTST=NINT(GETP('NTST',0,U))
       NCOL=NINT(GETP('NCOL',0,U))
       allocate(WI(0:NCOL))
       allocate(DTM(NTST))
       

 	do i=0,NCOL
	WI(i)=(GETP('WINT',i,U))
	end do
  	do i=1,NTST
	DTM(i)=(GETP('DTM',i,U))
	end do     
      
    PAR(5) =  GETU2(U,NDX,NTST,NCOL,WI,DTM,PAR) 
    PAR(7) =  GETU3(U,NDX,NTST,NCOL,WI,DTM,PAR) 


deallocate(DTM,WI)
     
!      DOUBLE PRECISION, INTENT(IN) :: U(NDIM)
!      DOUBLE PRECISION, INTENT(INOUT) :: PAR(*)
!
!      DOUBLE PRECISION, EXTERNAL :: GETP,GETU2
!      INTEGER NDX,NCOL,NTST
!---------------------------------------------------------------------- 
! NOTE : 
! Parameters set in this subroutine should be considered as ``solution 
! measures'' and be used for output purposes only.
! 
! They should never be used as `true'' continuation parameters. 
!
! They may, however, be added as ``over-specified parameters'' in the 
! parameter list associated with the AUTO-Constant NICP, in order to 
! print their values on the screen and in the ``p.xxx file.
!
! They may also appear in the list associated with AUTO-constant NUZR.
!
!---------------------------------------------------------------------- 
! For algebraic problems the argument U is, as usual, the state vector.
! For differential equations the argument U represents the approximate 
! solution on the entire interval [0,1]. In this case its values can
! be accessed indirectly by calls to GETP, as illustrated below, or
! by obtaining NDIM, NCOL, NTST via GETP and then dimensioning U as
! U(NDIM,0:NCOL*NTST) in a seperate subroutine that is called by PVLS.
!---------------------------------------------------------------------- 

! Set PAR(2) equal to the L2-norm of U(1)
!       PAR(2)=GETP('NRM',1,U)
!        NTST=NINT(GETP('NTST',0,U))
!        PAR(5) = (1./((1.+U(2))**2.) - 2./((1.+U(2))**1.))
! Set PAR(3) equal to the minimum of U(2)
!       PAR(3)=GETP('MIN',2,U)

! Set PAR(4) equal to the value of U(2) at the left boundary.
!       PAR(4)=GETP('BV0',2,U)

! Set PAR(5) equal to the value of U(2) at the left boundary using
! another method
!       NDX=NINT(GETP('NDX',0,U))
!       NTST=NINT(GETP('NTST',0,U))
!       NCOL=NINT(GETP('NCOL',0,U))
!       PAR(5)=GETU2(U,NDX,NTST,NCOL)
!---------------------------------------------------------------------- 
! The first argument of GETP may be one of the following:
!        'NRM' (L2-norm),     'MAX' (maximum),
!        'INT' (integral),    'BV0 (left boundary value),
!        'MIN' (minimum),     'BV1' (right boundary value).
!        'MNT' (t value for minimum)
!        'MXT' (t value for maximum)
!        'NDIM', 'NDX' (effective (active) number of dimensions)
!        'NTST' (NTST from constant file)
!        'NCOL' (NCOL from constant file)
!        'NBC'  (active NBC)
!        'NINT' (active NINT)
!        'DTM'  (delta t for all t values, I=1...NTST)
!        'WINT' (integration weights used for interpolation, I=0...NCOL)
!
! Also available are
!   'STP' (Pseudo-arclength step size used).
!   'FLD' (`Fold function', which vanishes at folds).
!   'BIF' (`Bifurcation function', which vanishes at singular points).
!   'HBF' (`Hopf function'; which vanishes at Hopf points).
!   'SPB' ( Function which vanishes at secondary periodic bifurcations).
!   'EIG' ( Eigenvalues/multipliers, I=1...2*NDIM, alternates real/imag parts).
!   'STA' ( Number of stable eigenvalues/multipliers).
!---------------------------------------------------------------------- 

END SUBROUTINE PVLS
